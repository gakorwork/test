package Problem;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ProblemasTest {

	@Test
	public void test() {
		
		// Puede comer a un enemigo
		int [][] board1 = {{0,0,0,0,0},{0,0,0,0,0},{0,0,1,0,0},{0,0,0,0,0},{0,2,0,0,0}};
		assertEquals(true,  Problemas.searchArea(board1));
		// No Puede comer a un enemigo
		int [][] board2 = {{0,0,0,0,0},{0,0,0,0,0},{0,0,1,0,0},{0,0,0,0,0},{0,0,2,0,0}};
		assertEquals(false,  Problemas.searchArea(board2));
		// No Puede comer a un enemigo
		int [][] board3 = {{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,1},{0,0,0,0,0},{0,2,0,0,0}};
		assertEquals(true,  Problemas.searchArea(board3));
		// Puede comer a un enemigo
		int [][] board4 = {{0,0,0,2,0},{0,0,0,0,0},{0,0,0,0,1},{0,0,0,0,0},{0,0,0,0,0}};
		assertEquals(true,  Problemas.searchArea(board4));
		// Puede comer a un enemigo
		int [][] board5 = {{0,0,0,0,0},{0,0,0,0,0},{1,0,0,0,0},{0,0,2,0,0},{0,0,0,0,0}};
		assertEquals(true,  Problemas.searchArea(board5));
	}
}
