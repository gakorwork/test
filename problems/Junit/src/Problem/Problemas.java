package Problem;

/**Brian Coronel <gakorwork@gmail.com>
 * @version 0
 * Caballo ajedrez
 */
import java.util.*;
public class Problemas {
	
	public static void main(String[] args) {
		Scanner read = new Scanner(System.in);

		int[][] chessBoard = new int[5][5];

		for (int i = 0; i < chessBoard.length; i++) {
			for (int j = 0; j < chessBoard[0].length; j++) {
				chessBoard[i][j] = read.nextInt();
			}
		}
		
		for (int i = 0; i < chessBoard.length; i++) {
			for (int j = 0; j < chessBoard[0].length; j++) {
				System.out.print(chessBoard[i][j] + " ");
			}
			System.out.println();
		}

		if (searchArea(chessBoard)) {
			System.out.println("Puede.");
		} else {
			System.out.println("No puede.");
		}

	}

	/**
	 * <p>
	 * Comprobar� donde est� el elemento solicitado
	 * </p>
	 * <p>
	 * y hara una busqueda del objetivo alrededor de �l
	 * </p>
	 * <p>
	 * con 2 casillas de distancia.
	 * </p>
	 * 
	 * @param matrix
	 * @return
	 */
	/**
	 * 
	 * @param matrix
	 * @param aliado
	 * @param objetivo
	 * @return
	 */

	public static boolean searchArea(int[][] matrix) {

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (matrix[i][j] == 1) {

					int startY = Math.max(0, i - 2); // si i-2 es igiaul a -2 devuelve 0 de esta manera si i-2 es igual a 1, devolvera 1.
					int startX = Math.max(0, j - 2);

					int endY = Math.min(matrix.length - 1, i + 2); // si i + 2 llega a ser 9 devuelve 7.
					int endX = Math.min(matrix[0].length - 1, j + 2);

					for (int k = startY; k <= endY; k++) {
						for (int l = startX; l <= endX; l++) {
							if (k == i - 2 && l == j - 1 || k == i - 2 && l == j + 1 || k == i - 1 && l == j + 2
									|| k == i + 1 && l == j + 2 || k == i + 2 && l == j + 1 || k == i + 2 && l == j - 1
									|| k == i + 1 && l == j - 2 || k == i - 1 && l == j - 2) {
								if (matrix[k][l] == 2) {
									return true;
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
}
